const apiList = {
    "url": document.location.protocol == "https:" ? "https://" + location.hostname + ":8000/api/" : "http://" + location.hostname + ":8000/api/",
    "api": {
        'confirm_phone': 'auth/confirm-phone',
        'reg': 'auth/reg',
        'login': 'auth/login',
        'reset_password': 'auth/reset',

        'client': 'client',
        'clubs': 'clubs',

        'trainers': 'trainers',
        'trainer_detail': 'trainers/detail',
        'training_canceled': 'training/cancel',

        'times': 'trainers/times',
        'trainers_by_times': 'trainers/filter',

        'shop': 'shop/products',
        'promocode_check': 'promocode/check',
        'training_sub_write': 'subscription/write',  // запись на тренеровку с абонемента
        'training_once_pay': 'subscription/write/once', // Оплата разовой тренеровки из заказа
        'training_shop_sub_pay': 'subscription/product/pay', // Оплата абонементов
        'training_reserved_payment': 'subscription/product/reserved', // Оплата со страницы тренеровки - бронирования

        'order_check': 'order/check',

        'image_upload': 'images/upload',
        'send_verified': 'verified/send'
    }
}

export { apiList }