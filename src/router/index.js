import { createRouter, createWebHistory } from 'vue-router'
import store from "../store/index"

import Main from "../views/Main.vue";
import Profile from "../views/Profile.vue";
import Subscription from "../views/Subscription.vue";
import Timetable from "../views/Timetable.vue";
import TimetableConfirm from "../views/TimetableConfirm.vue";
import Workout from "../views/Workout.vue";
import SuccessPay from "../views/SuccessPay.vue";
import ErrorWrite from "../views/ErrorWrite.vue";
import Registration from "../views/Registration.vue";
import Login from "../views/Login.vue";
import Logout from "../views/Logout.vue";
import Password from "../views/Password.vue";
import Verify from "../views/VerifiedPage.vue";

const routes = [
  { path: "/profile", component: Profile, name: "Profile", meta: { protect: true } },
  { path: "/verify", component: Verify, name: "VerifiedPage" },
  { path: "/main", component: Main, name: "Main", meta: { protect: true }  },
  { path: "/subscription", component: Subscription, name: "Subscription", meta: { protect: true } },
  { path: "/", component: Timetable, name: "Timetable" },
  { path: "/write-confirm", component: TimetableConfirm, name: "TimetableConfirm" },
  { path: "/workout", component: Workout, name: "Workout", meta: { protect: true }  },
  { path: "/success", component: SuccessPay, name: "SuccessPay", meta: { protect: true } },
  { path: "/canceled", component: ErrorWrite, name: "ErrorWrite", meta: { protect: true } },
  { path: "/auth/reg", component: Registration, name: "Registration" },
  { path: "/auth/login", component: Login, name: "Login" },
  { path: "/auth/logout", component: Logout, name: "Logout" },
  { path: "/auth/reset", component: Password, name: "Password" },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior (to, from, savedPosition) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(document.getElementById('app').scrollIntoView());
      }, 250)
    })
  }
})

router.beforeEach((to, from, next) => {
  if (to.meta.protect && !store.state.auth_status) next({ name: 'Login' })
  else next()
})

export default router
