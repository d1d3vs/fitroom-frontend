import { createStore } from 'vuex'
import Cookies from 'js-cookie'
import axios from "axios"
import { apiList } from '../api/api'
import { DateTime, Interval } from 'luxon'
import * as dayjs from 'dayjs'
import router from '../router'

const state = {
    preloader: false,
    locking_pool: 0, // Пулл axois запросов 

    app_loaded: false,
    base_loaded: false, // Данные о тренере загруженны
    trainers_reload: false, // Если выбрали другой офис - перезагружаем список
    timetable_redirect_confirm: false, // Идёт со строаницы записи? Это для переадресации после регистрации
    verify_redirect_confirm: false, // Если авторизация со страницы верификации
    popup_offer_status: false, // Окно офферты
    popup_politics_status: false, // Окно политики
    popup_politics_verified_status: false, // Окно политики - условий в верификации
    popup_offices_status: false,
    popup_office_local_status: false,
    popup_trainer_detail_status: false,
    //Вид тренеровки абонемента на который происходит запись
    subscription_write_type: null,
    // Окно подробнее об абонементе
    popup_sub_detail: {
        open: false,
        category: null
    },
    // Окно подтверждения верификации
    popup_verified: { 
        open: false,
        office_id: null
    },

    /* Статус авторизации */
    auth_status: false,
    utoken: null,
    menu_enabled: false,
    /* Авторизовывался раньше */
    auth_before: false,
    /* Подтвердил первый вход на сайт */
    first_visit_confirm: false,

    /* Работа со студиями */
    office_selected_id: null,
    office_selected_object: {
        id: null,
        address: null,
        city_code: null,
        city: null
    },

    /* Выбор студии для локального компонента */
    office_selected_local_changed: false,
    office_selected_local_id: null,
    office_selected_local_object: {
        id: null,
        address: null,
        city_code: null,
        city: null
    },
    
    /* Выбранный город */
    city_selected: false,
    /* Список тренеров в компоненте */
    trainers_list: [],
    /* Подробнее о тренере */
    trainer_detail_object: null,
    /* Даты и время тренера на выбранную дату */
    trainer_calendar_list: null,
    trainer_datetime_list: null,
    /* Выбран ли сейчас тренер */
    trainer_selected_status: null,
    timetable_write: {
        office_id: null,
        office_object: null,
        trainer_id: null,
        trainer_object: null,
        to_date: null,
        to_time: null,
        to_date_object: null
    },

    profile: {},
    offices: {
        cities: [],
        catalog: []
    },
    trainers: {},
    shop: {},
}

const mutations = {
    lockUi: (state) => state.locking_pool++,
    unlockUi: (state) => state.locking_pool--,

    loaded: (state, payload) => { state.app_loaded = payload },
    popup_offices_status: (state, payload) => { state.popup_offices_status = payload; },
    popup_office_local_status: (state, payload) => { state.popup_office_local_status = payload; },
    popup_trainer_detail_status: (state, payload) => { state.popup_trainer_detail_status = payload; },
    popup_sub_detail_open_set: (state, payload) => {
        state.popup_sub_detail = {
            open: true,
            category: payload
        }
    },

    popup_verified_set: (state, payload) => {
        state.popup_verified = {
            open: payload['open'],
            office_id: payload['office_id']
        }
    },

    first_visit_confirm_set: (state, payload) => { state.first_visit = payload; },

    timetable_to_date_set: (state, payload) => { state.timetable_write.to_date = payload; },
    timetable_to_date_object_set: (state, payload) => { state.timetable_write.to_date_object = payload; },
    timetable_to_time_set: (state, payload) => { state.timetable_write.to_time = payload; },

    trainer_detail_object_set: (state, payload) => { state.trainer_detail_object = payload; },
    trainer_calendar_list_set: (state, payload) => { state.trainer_calendar_list = payload; },
    trainer_datetime_list_set: (state, payload) => { state.trainer_datetime_list = payload; },
    office_selected_set: (state, item_office) => { 
        state.office_selected_id = item_office.id;
        state.office_selected_object = item_office;
    },

    office_selected_local_set: (state, item_office) => { 
        state.office_selected_local_id = item_office.id;
        state.office_selected_local_object = item_office;
    },

    profile_set: (state, payload) => { state.profile = payload },
    trainers_set: (state, payload) => { state.trainers = payload },
    offices_set: (state, payload) => { state.offices = payload },
    shop_set: (state, payload) => { state.shop = payload },

    login: (state, user_token) => {
        state.auth_status = true
        state.auth_before = true
        state.utoken = user_token
        state.menu_enabled = true
    },

    logout: (state, user_token) => {
        state.auth_status = false
        state.utoken = null
        state.menu_enabled = false
    },

    auth_before: (state, status) => {
        state.auth_before = true
    }
}

const actions = {
    async APP_INIT (context, payload) {
        await context.dispatch('LOGIN');
        console.log("APP_INIT")
        await context.dispatch('LOAD_OFFICES');

        if (store.state.office_selected_id) {
            context.dispatch('LOAD_TRAINERS');
            if (state.utoken)
                context.dispatch('LOAD_SHOP');
        } else {
            const params = new URLSearchParams(window.location.search)
            for (const param of params) {
                if (param[0] == 'club') {
                    let off_list = state.offices
                    for (let item_office of off_list['catalog']) {
                        if (item_office['id'] == param[1]) {
                            state.office_selected_id = param[1]
                            Cookies.set('office_selected_id', param[1]);

                            state.office_selected_object = {
                                id: item_office['id'],
                                address: item_office['name'],
                                city_code: null,
                                city: null
                            }

                            context.dispatch('APP_INIT')
                        }
                    }
                }
            }
            
            if (params[0] != 'club') {
                store.state.popup_offices_status = true
            }
        }

        if (state.utoken) 
            context.dispatch('load_profile')

        context.commit('loaded', true)
    },
    
    async LOGIN (context, user_token) {
        if (!user_token)
            if (Cookies.get('utoken') != null)
                user_token = Cookies.get('utoken')
            
        if (Cookies.get('auth_before'))
            context.commit('auth_before', true)

        if (user_token) {
            context.commit('login', user_token)
            Cookies.set('auth_before', true)
        }
            
    },

    LOGOUT: (context, user_token) => {
        let remove_cookie = Cookies.remove('utoken')
        context.commit('logout')
        context.dispatch('APP_INIT')
    },

    CANCEL_CONFIRM: (context, payload) => {
        // Отмена брони на сервере
    },

    async SHOP_SUB_PAYMENT (context, product) {
        context.commit('lockUi')

        let data_object = {
            'club_id': state.office_selected_id,
            'product_id': product['product_id'],
            'promocode': product['promocode']
        }

        let config = {
			headers: {
				"utoken": state.utoken,
            }
        }

        let url = apiList.url + apiList.api.training_shop_sub_pay
        return await axios.post(url, data_object, config).
            then(function (response) {
                if (response.data != null) {
                    if (response.data.result) {
                        // Делаем бронирование и переадресуем на оплату
                        let result = response.data.data
                        if (result)
                            window.location.href = result.formUrl
                        
                    } else {
                        console.log('Ошибка с оплатой абонемента, обратитесь в тех. поддержку')
                    }
                } else {
                    store.state.popup_sub_detail.open = false
                    store.commit('popup_verified_set', {'open': true, 'office_id': state.office_selected_id})
                }

                context.commit('unlockUi')
                return response;
            })
    },

    async TRAINING_RESERVED_PAYMENT (context, item) {
        context.commit('lockUi')
        let data_object = {
            'club_id': state.office_selected_id,
            'appointment_id': item.appointment_id
        }

        let config = {
			headers: {
				"utoken": state.utoken,
            }
        }

        let url = apiList.url + apiList.api.training_reserved_payment
        return await axios.post(url, data_object, config).
            then(function (response) {
                if (response.data.result) {
                    // Делаем бронирование и переадресуем на оплату
                    let result = response.data.data
                    if (Object.keys(result).length) {
                        window.location.href = result.formUrl
                    } else {
                        context.dispatch('APP_INIT')
                        router.push({'name': 'SuccessPay', 'query': {'type': item.category_type}})
                    }

                        
                } else {
                    alert('Ошибка с оплатой брони, обратитесь в тех. поддержку')
                }

                context.commit('unlockUi')
                return response;
            })
    },

    async TRAINING_RESERVED (context, item) {
        context.commit('lockUi')
        let training_type_split = item['training_type'].split(':')
        let tcategory = training_type_split[0]
        let ttype = training_type_split[1]

        let data_object = {
            'club_id': state.office_selected_id,
            'category': tcategory,
            'type': ttype,
            'employee_id': store.state.timetable_write.trainer_object['id'],
            'date': store.state.timetable_write.to_date,
            'time': store.state.timetable_write.to_time,
            'promocode': item['promocode']
        }

        let config = {
			headers: {
				"utoken": state.utoken,
            }
        }

        let url = apiList.url + apiList.api.training_once_pay
        return await axios.post(url, data_object, config).
            then(function (response) {
                if (response.data.result) {
                    // Делаем бронирование и переадресуем на оплату
                    let result = response.data.data
                    if (result.payment)
                        window.location.href = result.payment.formUrl
                    
                } else {
                    if (response.data.error == '1080') {
                        router.push({'name': 'ErrorWrite'})
                        context.dispatch('LOAD_TRAINERS')
                    }
                }

                context.commit('unlockUi')
                return response;
            })
    },

    async TRAINING_SUB_WRITE (context, ticket_id) {
        context.commit('lockUi')
        let data_object = {
            'club_id': state.office_selected_id,
            'ticket_id': ticket_id,
            'employee_id': store.state.timetable_write.trainer_object['id'],
            'date': store.state.timetable_write.to_date,
            'time': store.state.timetable_write.to_time
        }

        let config = {
			headers: {
				"utoken": state.utoken,
            }
        }

        let url = apiList.url + apiList.api.training_sub_write
        return await axios.post(url, data_object, config).
            then(function (response) {
                if (response.data.result) {
                    let result = response.data.data
                    router.push({'name': 'SuccessPay'})
                    // Обновляем данные пользователя
                    if (state.utoken) 
                        context.dispatch('load_profile')
                    
                    context.dispatch('LOAD_TRAINERS')
                } else {
                    if (response.data.error == '1080') {
                        router.push({'name': 'ErrorWrite'})
                        context.dispatch('LOAD_TRAINERS')
                    }
                }

                context.commit('unlockUi')
                return response;
            })
    },

    async LOAD_SHOP (context, payload) {
        context.commit('lockUi')

        let config = {
			headers: {
				"utoken": state.utoken,
            },
            params: {
                'club_id': state.office_selected_id
            }
        }

        let url = apiList.url + apiList.api.shop
        return await axios.get(url, config).
            then(function (response) {
                let data = response.data

                let shop = data.data
        
                context.commit('shop_set', shop);
                context.commit('unlockUi')
                return response;
            })
    },

    async PROMOCODE_CHECK (context, payload) {
        context.commit('lockUi')

        let config = {
			headers: {
				"utoken": state.utoken,
            },
            params: {
                'club_id': state.office_selected_id,
                'product_id': payload['product_id'],
                'promocode': payload['promocode']
            }
        }

        let url = apiList.url + apiList.api.promocode_check
        return await axios.get(url, config).
            then(function (response) {
                let data = response.data

                let promocode = data.data
        
                context.commit('unlockUi')
                return response;
            })
    },

    async LOAD_TRAINER_DETAIL (context, payload) {
        context.commit('lockUi')

        let config = {
			headers: {
				"utoken": state.utoken,
            },
            params: {
                'club_id': state.office_selected_id,
                'employee_id': state.timetable_write.trainer_id
            }
        }

        let url = apiList.url + apiList.api.trainer_detail
        return await axios.get(url, config).
            then(function (response) {
                let data = response.data

                let trainer_detail = data.data
        
                context.commit('trainer_detail_object_set', trainer_detail);
                context.commit('unlockUi')
                return response;
            })
    },

    async TRAINING_CANCELED (context, payload) {
        context.commit('lockUi')

        let config = {
            headers: {
                "utoken": state.utoken,
            },
            params: {
                'club_id': state.office_selected_id,
                'appointment_id': payload
            }
        }

        let url = apiList.url + apiList.api.training_canceled
        return await axios.get(url, config).
            then(function (response) {
                let data = response.data

                let result = data.result

                if (result) {
                    for (let item in store.state.profile.workouts.active) {
                        if (store.state.profile.workouts.active[item]['appointment_id'] == payload) {
                            store.state.profile.workouts.active.splice(item, 1)
                        }
                    }
    
                    for (let item in store.state.profile.workouts.reserved) {
                        if (store.state.profile.workouts.reserved[item]['appointment_id'] == payload) {
                            store.state.profile.workouts.reserved.splice(item, 1)
                        }
                    }

                    store.state.profile.workouts.count-= 1
                    context.dispatch('LOAD_TRAINERS')
                    context.dispatch('load_profile')
                }

                context.commit('unlockUi')
                return response;
            })
    },

    async LOAD_TRAINERS (context, payload) {
        context.commit('lockUi')

        let config = {
			headers: {
				"utoken": state.utoken,
				"club-id": state.office_selected_id,
            },
            
            params: {
                "club_id": state.office_selected_id
            }
        }

        //start_date=&end_date=&service_id=&club_id=
        let url = apiList.url + apiList.api.trainers
        
        return await axios.get(url, config).
            then(function (response) {
                let data = response.data
                
                if (data.result) {
                    let trainers = []
                    let office_object = {}
                    let trainers_json = data.data

                    for (let item of trainers_json.trainers) {
                        let name_split = item.name.replace(/\s+/g, ' ').trim().split(' ')

                        let fullname = item.name
                        let firstname = name_split[0]
                        let surname = name_split[1]
                        let type = item.position.id == null ? 'office' : 'trainer'
                        let photo = item.photo
                        let is_active = true

                        if (type == 'office') {
                            office_object = {
                                id: item.id,
                                office_id: state.office_selected_id,
                                type: type,
                                fullname: fullname,
                                firstname: firstname,
                                surname: surname,
                                photo: photo,
                                is_active: is_active,
                                calendar: item.calendar,
                                times: item.times,
                                date_times: item.date_times
                            }
                        } else {
                            trainers.push({
                                id: item.id,
                                office_id: state.office_selected_id,
                                type: type,
                                fullname: fullname,
                                firstname: firstname,
                                surname: surname,
                                photo: photo,
                                is_active: is_active,
                                calendar: item.calendar,
                                times: item.times,
                                date_times: item.date_times
                            })
                        }
                    }

                    let final = [office_object, ...trainers]
                    context.commit('trainers_set', final);

                    context.commit('unlockUi')
                    return response;
                } else {
                    context.commit('unlockUi')
                    return response;
                }
            })

    },

    async LOAD_OFFICES (context, payload) {
        context.commit('lockUi')

        let config = {
			headers: {
				"utoken": state.utoken,
			}
        }

        let url = apiList.url + apiList.api.clubs
        return await axios.get(url, config).
            then(function (response) {
                let data = response.data

                let offices = {
                    cities: [],
                    catalog: data
                }
        
                context.commit('offices_set', offices);
                context.commit('unlockUi')
                return response;
            })
    },

    async load_profile (context, payload) {
        context.commit('lockUi')

        let config = {
			headers: {
                "utoken": state.utoken,
                "club-id": state.office_selected_id
			}
        }
        
        let url = apiList.url + apiList.api.client
        return await axios.get(url, config).
            then(function (response) {
                if (!response.data.result) {
                    let data = response.data
                    if (data.error == 4005) {
                        context.dispatch('LOGOUT')
                        context.commit('unlockUi')
                        router.push({'name': 'Login'})
                    }
                } else {
                    let data = response.data
                    let profile = data.data
            
                    context.commit('profile_set', profile);

                    context.commit('unlockUi')
                    return response;
                }
            })
    }
}

const store = createStore({
    state,
    mutations,
    actions
})

export default store